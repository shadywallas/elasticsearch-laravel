<?php
/**
 * Part of the Sentry Social application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst Software Licence.
 *
 * @package    Sentry Social
 * @version    1.1
 * @author     Cartalyst LLC
 * @license    Cartalyst Software Licence - http://cartalyst.com/licence
 * @copyright  (c) 2011 - 2012, Cartalyst LLC
 * @link       http://cartalyst.com
 */



return array(
    
    /**
     * URL's
     */
    'hosts' => array(
        // request callback url
        'ip_port' => '192.168.0.101:9200',
        
        // register
        'ip' => '192.168.0.101:9200',
        
        // login page
        'domain_port' => 'localhost:9200',
        
        // authenticated
        'domain' => 'localhost'
    ),
    
    
)
;
