<?php


// Autoload classes
Autoloader::namespaces(array(
    'elastic' => Bundle::path('elastic'),
));

// map the auth controller to extend
Autoloader::map(array(
	'elastic_Controller' => __DIR__.DS.'controllers/auth.php',
));

Autoloader::alias('elastic\\elastic', 'elastic');
