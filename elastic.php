<?php
	/**
	 * Part of the Sentry Social application.
	 *
	 * NOTICE OF LICENSE
	 *
	 * Licensed under the Cartalyst Software Licence.
	 *
	 * @package    Sentry Social
	 * @version    1.1
	 * @author     Cartalyst LLC
	 * @license    Cartalyst Software Licence - http://cartalyst.com/licence
	 * @copyright  (c) 2011 - 2012, Cartalyst LLC
	 * @link       http://cartalyst.com
	 */
	namespace Elastic;

		use Config;
		use DB;
		use Exception;
		use Sentry;
		use Str;
		use Log;
		use seeker;
		use Session;
		use Resume;
		use member;

		class SentrySocialException extends Exception
		{
		}

		/**
		 * SentrySocial Auth class
		 *
		 * @package SentrySocial
		 * @author Daniel Petrie
		 */
		class Elastic
		{

			/**
			 *
			 * @var object OAuth/OAuth2 provider object
			 */
			protected $node = null;

			/**
			 *
			 * @var string what OAuth protocal is used ( OAuth/OAuth2 )
			 */
			protected $domain_port = null;

			/**
			 * Constructor
			 *
			 * @param
			 *            string social provider (facebook/twitter etc)
			 */
			public function __construct($node)
			{
				// grab the config and make sure it is set
				$this->domain_port = Config::get('Elastic::Elastic.hosts.domain_port');
				$this->node = $node;

			}

			/**
			 * Forge
			 *
			 * @param
			 *            string social provider (facebook/twitter etc)
			 */
			public function index($doc)
			{
				unset($doc['p11']);
				
				$url = "http://".$this->domain_port.'/'.$this->node.'/table';
				$options = array('http' => array(
						'header' => "Content-Type: application/x-www-form-urlencoded\r\n",
						'method' => 'POST',
						'content' => json_encode($doc)
					));
				$context = stream_context_create($options);
				$result = file_get_contents($url, false, $context);
				return json_decode($result);	
			}

		}
